#!/usr/bin/env bash

source "$(dirname "$0")/common.sh"

info "Executing the pipe..."

init_array_var() {
    local array_var=${1}
    local count_var=${array_var}_COUNT
    for (( i = 0; i < ${!count_var:=0}; i++ ))
    do
      eval ${array_var}[$i]='$'${array_var}_${i}
    done
}

# Required parameters
# (none)

# Default parameters
DEBUG=${DEBUG:="false"}
if [[ "$DEBUG" == "true" ]]; then
  enable_debug
fi
FAIL_ON_WARN=${FAIL_ON_WARN:="false"}
FAIL_ON_INFO=${FAIL_ON_INFO:="false"}
FAIL_ON_ERROR=${FAIL_ON_ERROR:="true"}
init_array_var "EXTRA_ARGS"
init_array_var "TEMPLATES"

cd "$BITBUCKET_CLONE_DIR"
# Run cfn-lint
run cfn-lint "${EXTRA_ARGS[@]}" -- "${TEMPLATES[@]}"

case "$status" in
    "0")
      success "cfn-lint reported no errors or warnings"
      ;;
    "2")
      fail "cfn-lint reported errors"
      ;;
    "4")
      if [[ "$FAIL_ON_WARN" == "false" ]]; then
        success "cfn-lint reported warnings"
      else
        fail "cfn-lint reported warnings"
      fi
      ;;
    "6")
      if [[ "$FAIL_ON_ERROR" == "false" ]]; then
        success "cfn-lint reported errors and warnings"
      else
        fail "cfn-lint reported errors and warnings"
      fi
      ;;
    "8")
      if [[ "$FAIL_ON_INFO" == "false" ]]; then
        success "cfn-lint reported infos"
      else
        fail "cfn-lint reported infos"
      fi
      ;;
    *)
      fail "Unknown cfn-lint exit code: ${status}"
      ;;
esac
