FROM python:3.7-alpine

RUN apk add --no-cache bash && \
    pip3 install cfn-lint==0.29.6

COPY pipe/ LICENSE.txt pipe.yml README.md /
ADD https://bitbucket.org/bitbucketpipelines/bitbucket-pipes-toolkit-bash/raw/0.4.0/common.sh /

RUN chmod a+x /*.sh

ENTRYPOINT ["/pipe.sh"]
