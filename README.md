# Bitbucket Pipelines Pipe: cfn-lint

This pipe contains [cfn-lint](https://pypi.org/project/cfn-lint/) ([cfn-python-lint](https://github.com/aws-cloudformation/cfn-python-lint)).

## YAML Definition

If you already have a `.cfnlintrc`, you just need to the following snippet to the script section of your `bitbucket-pipelines.yml` file:

```yaml
script:
  - pipe: jacksgt/cfn-lint-pipe:0.1.1
```


## Variables

| Variable              | Usage                                                       |
| --------------------- | ----------------------------------------------------------- |
| TEMPLATES             | Cloudformation templates to be checked. Default: `[]` (will use configuration in .cfnlintrc). |
| DEBUG                 | Turn on extra debug information. Default: `false`. |
| FAIL_ON_ERROR         | Fail the pipeline when cfn-lint finds errors. Default: `true`. |
| FAIL_ON_ERROR         | Fail the pipeline when cfn-lint reports errors. Default: `true`. |
| FAIL_ON_WARN         | Fail the pipeline when cfn-lint reports warnigns. Default: `true`. |
| FAIL_ON_INFO         | Fail the pipeline when cfn-lint reports infos. Default: `true`. |
| EXTRA_ARGS            | Extra command line arguments to be supplied to cfn-lint. Default: `[]`. |

_(*) = required variable._

## Examples

Advanced example with strict configuration:

```yaml
script:
  - pipe: jacksgt/cfn-lint-pipe:0.1.1
    variables:
      TEMPLATES:
        - cloudformation-template-A.json
        - cloudformation-template-B.yaml
      FAIL_ON_WARN: "true"
      FAIL_ON_INFO: "true"
```

## Support

If you have an issue with this pipe or would like to have a feature added, let me know.
Patches are also always welcome.

`jackdev@mailbox.org`

If you’re reporting an issue, please include:

- the version of the pipe
- relevant logs and error messages
- steps to reproduce

## License

This software is licensed under Apache 2.0 License.
